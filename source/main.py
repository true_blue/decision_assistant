"""This is the main program file"""


def get_options_from_user() -> list:
    """get all options from user then return the list of options"""
    print("enter the options you want to decide between, separated by commas:")
    raw_options: str = input()
    # start as set to remove dupes, then convert to list for subscripting
    return list({option.strip() for option in raw_options.split(",")})


def reduce_options_to_favorite(options) -> str:
    """have user compare options 2 at a time until 1 option remains,
    then return that option"""
    print("let's compare the options in pairs to find your favorite")
    print("enter the number of your choice")
    while len(options) > 1:
        option_1: str = options[0]
        option_2: str = options[1]
        print("which do you like better:")
        print(f"1. {option_1}")
        print(f"2. {option_2}")
        user_input = input()
        if user_input == "1":
            options.remove(option_2)
        elif user_input == "2":
            options.remove(option_1)
        else:
            print('enter "1" or "2"')
    return options[0]


print(f"your favorite option is: {reduce_options_to_favorite(get_options_from_user())}")
